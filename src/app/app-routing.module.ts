import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './component/home/home.component';
import {AddContactComponent} from './component/add-contact/add-contact.component';
import {ListOfContactsComponent} from './component/list-of-contacts/list-of-contacts.component';
import { ContactDetailsComponent } from './component/contact-details/contact-details.component';

const routes: Routes = [
  {path : "home", component : HomeComponent, pathMatch: 'full'},
  {path : "add", component : AddContactComponent},
  {path : "contacts", component : ListOfContactsComponent},
  {path : 'contacts/:id', component: ContactDetailsComponent },
  {path : 'edit/:id', component: AddContactComponent, pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
