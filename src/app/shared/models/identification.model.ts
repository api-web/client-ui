export class Identification {
    id: number;
    firstName : String;
    lastName : String;
    dateOfBirth : String;
    gender: String;
    title : String;
}
