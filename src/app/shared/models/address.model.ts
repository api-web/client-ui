export class Address {
 type : String;
 number : Number;
 street : String;
 unit : String;
 city : String;
 state : String;
 zipCode: String;
}
