import { Identification } from "./identification.model";
import { Address } from "./address.model";
import { Communication } from "./communication.model";

export class Contact {
    Identification : Identification;
    Address : Array<Address>;
    Communication : Array<Communication>;
    
}
