import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Contact } from '../../shared/models/contact.model';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class ContactService {
  // base url
  apiURL = '/api';

  
  // inject the HttpClient as http so we can use it in this class
  constructor(private http: HttpClient) {}
  
   // get list of contacts
   getContacts(): Observable<any> {
    var data = this.http.get( this.apiURL + '/contacts');
    data.subscribe(data => console.log(data));
    return data;
  }

  // get list of contact by id
  getContactByid(id : number): Observable<Contact> {
   return this.http.get<Contact>( this.apiURL + `/contact/${id}`);
  }

   // add contact
   addContact(contact): Observable<Contact> {
    console.log(JSON.stringify(contact));
    return this.http.post<Contact>(this.apiURL + '/contact', contact);
  
  }  
   // update Contact
  updateContact(contact): Observable<Contact> {
   return this.http.put<Contact>(this.apiURL + '/contact', contact );
   
 }  
  
  // delete contact
  deleteContact(id): Observable<any>{
    return this.http.delete(this.apiURL +  `/contact/${id}`);
  }


  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'text/plain; charset=utf-8'

    })
  }
}
 