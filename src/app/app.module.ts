import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddContactComponent } from './component/add-contact/add-contact.component';
import { ListOfContactsComponent } from './component/list-of-contacts/list-of-contacts.component';
import { HomeComponent } from './component/home/home.component';
import { ContactDetailsComponent } from './component/contact-details/contact-details.component';


//import { ContactService } from './contact.service';


@NgModule({
  declarations: [
    AppComponent,
    AddContactComponent,
    ListOfContactsComponent,
    HomeComponent,
    ContactDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule 
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
