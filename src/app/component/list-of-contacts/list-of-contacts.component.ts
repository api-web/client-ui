import { Component, OnInit } from '@angular/core';
import { ContactService } from '../../core/service/contact.service';
import { Observable } from 'rxjs';
import { Identification } from '../../shared/models/identification.model';

@Component({
  selector: 'app-list-of-contacts',
  templateUrl: './list-of-contacts.component.html',
  styleUrls: ['./list-of-contacts.component.css']
})
export class ListOfContactsComponent implements OnInit {

  // declare contacts
  identification : Observable<Identification>;
  title = 'List of Contacts';
  constructor(public contactService : ContactService) {}

  ngOnInit() {

    this.getList();
  }

  // retrieve the list and push on the object
  getList(){
    this.identification = this.contactService.getContacts();
    console.log(this.identification);
  }

  // delete contact
  deleteContact(id : number){
   
    var confirmMsg = confirm("Are you sure you want to delete this?");

      if (confirmMsg == true) {
        this.contactService.deleteContact(id).subscribe(data => {
       
        });
        this.getList();
      }
     
    }

 
    
}

  

