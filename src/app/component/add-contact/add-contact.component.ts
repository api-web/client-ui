import { Component, OnInit } from '@angular/core';
import { FormGroup,  FormBuilder,  Validators, FormControl } from '@angular/forms';
import { Contact } from '../../shared/models/contact.model';
import { Address } from '../../shared/models/address.model';
import { contactMsg } from '../../shared/contants/message/contactMsg';
import { Communication } from '../../shared/models/communication.model';
import { Identification } from '../../shared/models/identification.model';
import { ContactService } from '../../core/service/contact.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-contact',
  templateUrl: './add-contact.component.html',
  styleUrls: ['./add-contact.component.css']
})
export class AddContactComponent implements OnInit {

  identificationFrm: FormGroup;
  addressFrm: FormGroup;
  communicationFrm : FormGroup;
  
  err_message : String =  contactMsg.REQUIRED_FIELD;
  frm_message ='';

  Gender :any = ["Male", "Female"];
  
  // objects
  addressObj : Address[] = [];
  communicationObj : Communication[] =[];

  identification = new Identification();
  address = new Address();
  communication = new Communication();
 
  contact = new Contact();

  //count address
  countAddress = 0;
  countCom = 0;

  // flag
  ishidden = true;
  isUpdate = false;

  // declare id
  id: number;
  
  constructor(private fb: FormBuilder, 
              public contactService : ContactService,
              public router : Router,
              private route: ActivatedRoute){ 
  }


   // initialize identification fields
   identificationForm(){
    var male = "Male";
    var dob = (new Date().toISOString().substring(0,10));
    this.identification.gender = male;
    this.identification.dateOfBirth = dob;

    this.identificationFrm = this.fb.group({
      fname: ['', Validators.required ],
      lname: ['', Validators.required ],
      dob:  dob,
      title: ['', Validators.required ],
      gender: [male, Validators.required]

    });

   }

  // initialize address fields
  addressForm(){
    this.addressFrm = this.fb.group({
      addressType: ['', Validators.required ],
      cno: ['', [Validators.required, Validators.maxLength(11)]],
      street: ['', Validators.required ],
      unit: ['', Validators.required ],
      city: ['', Validators.required ],
      state: ['', Validators.required ],
      zipCode: ['', Validators.required ]

    });
    
  }

  // initialize address fields
  communicatonForm(){
    //set defaul value for preferred in comm
    this.communication.preferred = true;

    this.communicationFrm = this.fb.group({
      ctype: ['', Validators.required ],
      val: ['', Validators.required ],
     preferred: [true, Validators.required] 
    });
    
    
  }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    //identification
    this.identificationForm(); 
    //address
    this.addressForm();
    // communication
    this.communicatonForm();
    
    // only excecute in update form
   if(this.id){
     this.isUpdate = true;
     this.getContactDetail();
   }
  }

  // get the contact details
  getContactDetail(){
    
    this.contactService.getContactByid(this.id).subscribe(data =>{


      //identification
      this.identificationFrm.get("fname").patchValue(data.Identification.firstName);
      this.identificationFrm.get("lname").patchValue(data.Identification.lastName);
      this.identificationFrm.get("dob").patchValue(data.Identification.dateOfBirth);
      this.identificationFrm.get("gender").patchValue(data.Identification.gender);
      this.identificationFrm.get("title").patchValue(data.Identification.title);
    

      // address
      this.addressObj = data.Address;
      // communication
      this.communicationObj = data.Communication;
      this.countList();
    });

  }
 
  //count address list
  countList(){
    this.countAddress = this.addressObj.length;
    this.countCom = this.communicationObj.length;
  }

  // add address
  addAddress(){
    this.ishidden = true;
    this.addressObj.push(this.address);
    this.address = new Address();
    this.addressForm();
    this.countList();
  }

  // delete address
  deleteAddress(id: number){
    this.ishidden = true;
    this.addressObj.splice(id,1);
    this.countList();
  }

  // add address
  addCommunication(){
    this.ishidden = true;
    this.communicationObj.push(this.communication);
    this.communication = new Communication();
    this.communicatonForm();
    this.countList();
  }

  // delete address
  deleteCommunication(id: number){
    this.ishidden = true;
    this.communicationObj.splice(id,1);
    this.countList();
  }


  //push the data to Contact
  saveContact(){ 
 
    this.contact.Identification = this.identification;
    this.contact.Address = this.addressObj;
    this.contact.Communication = this.communicationObj;

    var service; 
    // check if for add or update
    if(this.isUpdate){
      // set identification id
      this.contact.Identification.id = this.id;
      service = this.contactService.updateContact(this.contact); 

    }else{
      service = this.contactService.addContact(this.contact);
    }
   
    service.subscribe(data => {
    
      alert("Successfully Saved!");
      console.log(data);
      this.back();
    });
   
   
  }


  // sumbit form
  onSubmit() {
    this.ishidden = true;
    // check address and communication object atlest 1 data and identitification is valid
    if(this.countAddress > 0 && this.countCom > 0 
        && this.identificationFrm.valid ){

          //save
          this.saveContact();

    }else{
      this.ishidden = false;
      this.frm_message = contactMsg.FRM_ERROR_MESSAGE;
      
    }
    window.scrollTo(0,0);
  }

  // update the contact
  onUpdate(){
    this.ishidden = true;
    // check address and communication object atlest 1 data and identitification is valid
    if(this.countAddress > 0 && this.countCom > 0 
        && this.identificationFrm.valid ){

          //save
          this.saveContact();

    }else{
      this.ishidden = false;
      this.frm_message = contactMsg.FRM_ERROR_MESSAGE;
      
    }
    window.scrollTo(0,0)
  }
   

  back(){
    this.router.navigate(['/contacts']);
  }

}

