import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactService } from '../../core/service/contact.service';
import { Observable } from 'rxjs';
import { Contact } from '../../shared/models/contact.model';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit {

  //declare contact
  contact : Contact;
  constructor( private route: ActivatedRoute, public contactService : ContactService, private router : Router) { }

  ngOnInit() {
    this.getDetail();
  }

  getDetail(){

    this.route.paramMap.subscribe(params => {
      this.contactService.getContactByid(+params.get('id')).subscribe(data =>{
        this.contact =data;
        console.log(this.contact);
      });
    });
  }

  back(){
    this.router.navigate(['/contacts']);
  }

}
